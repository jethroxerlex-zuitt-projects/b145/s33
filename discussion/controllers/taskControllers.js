const Task = require('../models/taskSchema');


//Retrieving all task
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
}

//Creating a task

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((task,err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return task
		}
	})
}
//DELETING A TASK
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask,err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

//UPDATING A TASK

module.exports.updateTask = (taskId,reqBody) => {
	return Task.findById(taskId).then((result,err) => {
		if(err){
			console.log(err)
			return false
		} 
		result.name = reqBody.name
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}

//ACTIVITY

//GETTING A SPECIFIC TASK

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((task,err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

//CHANGE STATUS OF TASK

module.exports.completedTask = (taskId,reqBody) => {
	return Task.findById(taskId).then((result,err) => {
		if(err){
			console.log(err)
			return false
		} 
		result.status = reqBody.status
		return result.save().then((completedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return completedTask
			}
		})
	})
}
