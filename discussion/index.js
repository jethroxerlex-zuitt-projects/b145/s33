const express = require ('express');
const mongoose = require ('mongoose');
const taskRoutes = require('./routes/taskRoutes')

const app = express();

const port = 4000;


app.use (express.json())
app.use(express.urlencoded({extended:true}))

//Middlewares
mongoose.connect("mongodb+srv://jramos:aketch24@wdc028-course-booking.il1nt.mongodb.net/session33?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error",console.error.bind(console, "There is an error with the connection"))
db.once("open",() => console.log("Successfully connected to the database"));


app.use('/tasks', taskRoutes)

app.listen(port,()=> console.log(`Server running at port ${port}`))